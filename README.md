# Bookshelves · Documentation

[![nuxtjs](https://img.shields.io/static/v1?label=nuxt/content&message=v2.15&color=00C58E&style=flat-square&logo=nuxt.js&logoColor=ffffff)](https://content.nuxtjs.org/themes/docs)
[![nuxtjs](https://img.shields.io/static/v1?label=Designed%20to%20be&message=SSG&color=00C58E&style=flat-square&logo=nuxt.js&logoColor=ffffff)](https://nuxtjs.org/docs/concepts/static-site-generation/)

[![NodeJS](https://img.shields.io/static/v1?label=NodeJS&message=v16.14&color=339933&style=flat-square&logo=node.js&logoColor=ffffff)](https://nodejs.org/en)
[![npm](https://img.shields.io/static/v1?label=NPM&message=v8&color=CB3837&style=flat-square&logo=npm&logoColor=ffffff)](https://docs.npmjs.com/cli/)

> Deploy on Netlify: [bookshelves-documentation.netlify.app](https://bookshelves-documentation.netlify.app/)
> Deploy on [documentation.bookshelves.ink](https://documentation.bookshelves.ink/)

## Setup

Install dependencies:

```bash
yarn
```

## Development

```bash
yarn dev
```

## Static Generation

This will create the `dist/` directory for publishing to static hosting:

```bash
yarn generate
```

To preview the static generated app, run `yarn start`

For detailed explanation on how things work, checkout [nuxt/content](https://content.nuxtjs.org) and [@nuxt/content theme docs](https://content.nuxtjs.org/themes-docs).

- <https://www.netlifycms.org/docs/nuxt>
- <https://www.netlify.com/blog/2020/04/20/create-a-blog-with-contentful-and-nuxt>
