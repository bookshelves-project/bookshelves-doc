---
title: 'Requirements: installation'
description: 'How to setup your environement to work on Bookshelves'
position: 4
category: 'Getting started'
---

## Languages

### PHP

- Linux: [computingforgeeks.com (Ubuntu)](https://computingforgeeks.com/how-to-install-php-on-ubuntu-2/) or [computingforgeeks.com (Debian)](https://computingforgeeks.com/how-to-install-php-on-debian-linux/) with `apt`
- macOS: [stitcher.io](https://stitcher.io/blog/php-81-upgrade-mac) with `homebrew`
- Windows: [scoop.sh](https://scoop.sh/) and PHP bucket [github.com/ScoopInstaller/PHP](https://github.com/ScoopInstaller/PHP) with `scoop`

### Node.js

- Linux: [github.com/nvm-sh/nvm](https://github.com/nvm-sh/nvm) with `nvm`
- macOS: [formulae.brew.sh/formula/nvm](https://formulae.brew.sh/formula/nvm) with `homebrew`
- Windows: [scoop.sh](https://scoop.sh/) and package `nvm` with `scoop`

### MySQL

- Linux: [digitalocean.com (Ubuntu)](https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-20-04) or [computingforgeeks.com (Debian)](https://computingforgeeks.com/how-to-install-mariadb-on-debian/) with `apt`
- macOS: [flaviocopes.com](https://flaviocopes.com/mysql-how-to-install/) with `homebrew`
- Windows: [scoop.sh](https://scoop.sh/) with package `mysql`

## PHP extensions

You will need to install `php8.1-xml` to manage `xml` files and `php8.1-gd` to manage images.

```bash
sudo apt-get install -y php8.1-xml php8.1-gd
```

## Image tools

For [`spatie/media-library`](https://spatie.be/docs/laravel-medialibrary/v10/introduction) can optimize images, you could have to install some tools for `production` server.

```bash
sudo apt-get install -y jpegoptim optipng pngquant gifsicle webp
```

```bash
npm install -g svgo
```

## PDF support <badge>optional</badge>

PDF can be used as format but to extract cover from PDF, you need to have [**ImageMagick**](https://imagemagick.org/index.php). So you have to install locally `imagemagick`

```bash
sudo apt install -y imagemagick
```

And you have to install PHP [`imagick`](https://pecl.php.net/package/imagick) to use ImageMagick from `pecl` or manually by downloading .tgz file.

```bash
sudo pecl install imagick
```

Check if PHP can use ImageMagick

```bash
php -m | grep imagick
```

You have to get this result `imagick`

## CBR support <badge>optional</badge>

- [Documentation](https://www.php.net/manual/en/rar.installation.php)

CBR files can be used only if your system can manage `.rar` files

```bash
sudo apt install -y rar
```

And you have to install PHP [`rar`](https://pecl.php.net/package/rar) from `pecl`

```bash
sudo pecl install rar
```

### Troubles with `rar`

...or, *if you have some errors*, manually by clone [**this repository**](https://github.com/cataphract/php-rar)

```bash
cd ~
git clone https://github.com/cataphract/php-rar
cd php-rar
phpize
./configure
make
make install
cd ..
```

If you haven't errors, you could find `~/php-rar/modules/rar.so`. Now you can add manually `rar.so` to `pecl`.

Find `pecl` directory for PHP

```bash
pecl config-get ext_dir
```

And just copy `rar.so` into this directory

```bash
cp ~/php-rar/modules/rar.so /usr/local/lib/php/pecl/YOUR_VERSION
```

Now, you have to find your `php.ini`

```bash
php --ini
```

For example, I've this result `Loaded Configuration File: /usr/local/etc/php/8.1/php.ini`

You can edit it

```bash
vim /your/path/to/php.ini
```

You can search extension section or just add at the end

```ini[php.ini]
extension=rar.so
```

Check if your `extension_dir` have right value : `extension_dir = "/usr/local/lib/php/pecl/YOUR_VERSION"`.

And now you can restart PHP services.

```bash
brew services restart php@8.1
```

You can remove `php-rar`

```bash
rm -r ~/php-rar
```

### Check if `rar` works

To know if PHP can use `.rar` files, just use

```bash
php -m | grep rar
```

You have to get this result: `rar`

## Package managers

### pnpm

<badge>requirement</badge> `npm`

```bash
npm i -g pnpm
```

### Composer

<badge>requirement</badge> `php`

You can download `composer` from [**official website**](https://getcomposer.org/download/)

```bash
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
# check SHA
php composer-setup.php
php -r "unlink('composer-setup.php');"
```

Add to `bin`

```bash
sudo mv composer.phar /usr/local/bin/composer
sudo chown -R $USER ~/.config/composer/
```

Add to PATH

```bash
export PATH=~/.config/composer/vendor/bin:$PATH
```
