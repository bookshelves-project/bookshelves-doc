---
title: Commands
position: 4
category: Back
---

Commands have some options, use `-h` to get list of all options.

## `bookshelves:scan`

You can scan `books` directory to get a list of your EPUB files to know if everything works.

> -v is for verbose to get each file

```bash
php artisan bookshelves:scan -v
```

## `bookshelves:generate`

**Main command of Bookshelves**, can parse all eBooks and **detect new eBooks** *OR* remove all eBooks data (and keep accounts) with `--fresh` like `php artisan bookshelves:generate -f`. When you want to detect new eBooks, just launch command without any option. It will launch multiple commands:

- `bookshelves:books`: parse all EPUB files, extract data, create relations and generate covers **if not exist**
- `bookshelves:series`: parse all series into database (don't create series) to generate cover and extra data **if haven't extra data**
- `bookshelves:authors`: parse all authors into database (don't create authors) to generate cover and extra data **if haven't extra data**

All these commands try to get extra data from Internet (Wikipedia and GoogleBooks), use `--local` like `php artisan bookshelves:generate -L` option to skip this feature.

>{--e|erase : erase all data}  
>{--f|fresh : reset current books and relation, keep users}  
>{--F|force : skip confirm question for prod}  
>{--L|local : prevent external HTTP requests to public API for additional informations}  
>{--d|debug : generate metadata files into public/storage/debug for debug}  
>{--t|test : execute tests at the end}  
>{--A|skip-admin : skip admin and roles generation}  
>{--l|limit= : limit epub files to generate, useful for debug}  

**WARNING**  
*`--l|limit=` option have to be set at the end of options*

*Example: here command will check only new eBooks*

```bash
php artisan bookshelves:generate
```

*Example: here command will*

- *erase all data with `migrate:fresh` with `-e` from bookshelves:books*
- *check all eBooks and erase books with relationships with `-f` from bookshelves:books*
- *books assets with `-b` from bookshelves:assets*
- *authors assets with `-a` from bookshelves:assets*
- *series assets with `-s` from bookshelves:assets*
- *comments with `-C` from bookshelves:sample*
- *selection with `-S` from bookshelves:sample*

```bash
php artisan bookshelves:generate -efbasCS
```

*Example: here command will use only local data with `-L`, get only 100 first EPUB files with `-l=100`*

```bash
php artisan bookshelves:generate -Ll=100
```

#### books

```bash
php artisan bookshelves:books
```

#### series

```bash
php artisan bookshelves:series
```

#### authors

```bash
php artisan bookshelves:authors
```

#### clear

```bash
php artisan bookshelves:clear
```

### *b. sample*

#### sample account data

```bash
php artisan bookshelves:sample
```

#### sample books

If you want to test Bookshelves, you can use `bookshelves:sample` to generate data from libre eBooks

```bash
php artisan bookshelves:sample-books
```

## Tests

```bash
php artisan bookshelves:test
```

```bash
php artisan pest
```

```bash
php artisan larastan
```

### *b. Setup*

```bash
php artisan setup
```

### *c. Misc*

```bash
php artisan log:clear
```

```bash
php artisan log:read
```
