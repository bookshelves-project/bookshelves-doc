---
title: Introduction
subtitle: A library to rule them all, a library to find them. A library to bring them all, and in the shelves bind them. 
position: 1
category: 'Getting started'
features:
  - Extract metadata of EPUB and CBZ files
  - "Full relationships for every book or comic : authors, publisher, date of release, language, identifiers, tags, description, cover, serie"
  - Detailed API to interact with front or mobile application
  - Documentation
  - OPDS feed
  - Catalog, a basic interface for eReader web browser to download eBooks
  - Webreader, a small interface to read eBooks into a web browser
  - Front-end fully typescripted with books, series, authors and tags with filter and sort options, i18n and dark mode included
  - Search engine with Laravel Scout powered by Meilisearch
  - Account feature to save favorites and write comments
---

<img src="/preview.png" class="light-img" width="1280" height="640" alt=""/>
<img src="/preview-dark.png" class="dark-img" width="1280" height="640" alt=""/>

Bookshelves offers to create a web application with an documented API **to present your eBooks** (EPUB) **and your comics** (CBZ). You can check a **Bookshelves demo** at [bookshelves.ink](https://bookshelves.ink).

<alert type="warning">

Currently in beta.

</alert>

This documentation is about Bookshelves project, you will find two parts covered here: the **back-end** part made in **Laravel** (PHP) which is clearly **the most important part** in Bookshelves and the **front-end** part in **NuxtJS** (Vue.js) which retrieves data from the API in order to display it in a nice user interface.

If you are interested in Bookshelves, you can keep only the back-end part and create your own front-end with the technology you want. *All the logic of Bookshelves is in the backend* and it is even *possible to not use an external frontend* and use Bookshelves with the *internal backend interface* named *Catalog*.
## Features

<list :items="features"></list>

## Repositories

- 📀 [bookshelves-back](https://gitlab.com/bookshelves-project/bookshelves-back) : back-end with *Laravel v9* in **beta**
- 🎨 [bookshelves-front](https://gitlab.com/bookshelves-project/bookshelves-front) : front-end with *NuxtJS v3* in **beta**
- 📚 [bookshelves-doc](https://gitlab.com/bookshelves-project/bookshelves-doc) : documentation with *nuxt/content*
- 📱 [bookshelves-mobile](https://gitlab.com/bookshelves-project/bookshelves-mobile) : mobile application with *Flutter v2* in **alpha**
