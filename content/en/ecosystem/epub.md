---
title: EPUB
position: 1
category: Ecosystem
---

*EPUB* is a standard format with `.epub` extension and `application/epub`/`application/epub+zip` MIME type. It's basically just a zip file with modified extension and specific architecture of files.

- TODO main files
- TODO read epub
- TODO series, calibre

## Definition

*From [Wikipedia](https://en.wikipedia.org/wiki/EPUB)*:

>EPUB is an e-book file format that uses the ".epub" file extension. The term is short for electronic publication and is sometimes styled ePub. EPUB is supported by many e-readers, and compatible software is available for most smartphones, tablets, and computers. EPUB is a technical standard published by the International Digital Publishing Forum (IDPF). It became an official standard of the IDPF in September 2007, superseding the older Open eBook standard.
>
>The Book Industry Study Group endorses EPUB 3 as the format of choice for packaging content and has stated that the global book publishing industry should rally around a single standard. The EPUB format is implemented as an archive file consisting of XHTML files carrying the content, along with images and other supporting files. EPUB is the most widely supported vendor-independent XML-based (as opposed to PDF) e-book format; that is, it is supported by almost all hardware readers, except for Kindle.

## OPF 2.0.1

Bookshelves support **Open Packaging Format 2.0.1**, [details are here](https://en.wikipedia.org/wiki/EPUB#Open_Container_Format_2.0.1) with `.opf` file.

```xml[content.opf]
<?xml version="1.0"  encoding="UTF-8"?>
<package xmlns="http://www.idpf.org/2007/opf" version="2.0" unique-identifier="uuid_id">
  <metadata xmlns:opf="http://www.idpf.org/2007/opf">
    <dc:title>Le clan de l'ours des cavernes</dc:title>
    <dc:creator opf:role="aut" opf:file-as="Auel, Jean M.">Jean M. Auel</dc:creator>
    <dc:contributor opf:role="bkp">calibre (5.14.0) [https://calibre-ebook.com]</dc:contributor>
    <dc:description>La petite orpheline Cro-Magnon recueillie...</dc:description>
    <dc:publisher>Presses de la cité</dc:publisher>
    <dc:identifier id="uuid_id" opf:scheme="uuid">f1289cf7-f2bf-4af7-8795-bacc1bc765aa</dc:identifier>
    <dc:date>1980-01-13T21:00:00+00:00</dc:date>
    <dc:subject>Roman Historique</dc:subject>
    <dc:language>fr</dc:language>
    <dc:identifier opf:scheme="calibre">f1289cf7-f2bf-4af7-8795-bacc1bc765aa</dc:identifier>
    <dc:identifier opf:scheme="ISBN">9782266122122</dc:identifier>
    <dc:identifier opf:scheme="GOOGLE">63CTHAAACAAJ</dc:identifier>
    <meta name="calibre:series" content="Les Enfants de la Terre"/>
    <meta name="calibre:series_index" content="1.0"/>
  </metadata>
  <manifest>
    <item id="cover" href="cover1.jpeg" media-type="image/jpeg"/>
  </manifest>
</package>
```

### Book conversion

After parsing XML, it will be converted to a [Book entity](/back/entities-relationships#book).

- `title` from `<dc:title />`
- `contributor` from `<dc:contributor />`, can be multiple, `array` will be converted to `string`
- `description` from `<dc:description />`
- `released_on` from `<dc:date />`
- `rights` from `<dc:rights />`
- `volume` from `<meta name="calibre:series_index" />`
- `identifiers` from `<dc:identifier />` can be multiple, will be converted to `array`
  - `isbn10` from `<dc:identifier opf:scheme="isbn" />` TODO
  - `isbn13` from `<dc:identifier opf:scheme="isbn" />` TODO
- `medias.cover` from `<manifest />` node in `<item />` TODO
- `authors` from `<dc:creator />` can be multiple, will be converted to `array`
- `language` from `<dc:language />`
- `serie` from `<meta name="calibre:series" />`
- `publisher` from `<dc:publisher />`
- `tags` from `<dc:subject />` can be multiple, will be converted to `array`
