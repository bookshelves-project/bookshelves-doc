---
title: Calibre
position: 2
category: Ecosystem
---

*From Calibre website*:

> Calibre is a powerful and easy to use e-book manager. Users say it’s outstanding and a must-have. It’ll allow you to do nearly everything and it takes things a step beyond normal e-book software. It’s also completely free and open source and great for both casual users and computer experts.

You can download Calibre [here](https://calibre-ebook.com/), available on Linux, macOS and Windows.

TODO update ebook
TODO series
