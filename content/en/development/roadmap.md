---
title: Roadmap
subtitle: Next updates
description: ''
position: 2
category: 'Development'
---

This is roadmap for development, all planned features will be listed here. You can add an issue here to ask for new feature from <https://gitlab.com/groups/bookshelves-project/-/issues>.

## *Ideas*

### Back-end

- [ ] `i18n` for back features
- [ ] Testing

### Front-end

*EMPTY*

## *Planned*

### Back-end

*EMPTY*

### Front-end

*EMPTY*

## *In progress*

### Back-end

- [ ] Improve OPDS
- [ ] Logs for EpubParser
- [ ] Authors notes
- [ ] Improve Webreader

### Front-end

- [ ] Add `i18n`
- [ ] Review each component, remove `import`
- [ ] Improve advanced search
- [ ] `nuxt 3` migration
- [ ] Improve `jsonld`, `sitelinks`
- [ ] Cookie consent
- [ ] Pagination related
- [ ] `api` related more logic: not with books
- [ ] Book `slug` button couple in header color
- [ ] More metadata
- [ ] Author `slug` books/series load more
- [ ] Author/book/serie slug mixin
- [ ] Toasts bus
- [ ] Serie/author load mounted books/series

## *Done*

### Back-end

- [x] Meilisearch to replace native search if needed (native can be used without Meilisearch)
- [x] Rewrite eBook parser engine
- [x] Datatables
- [x] API doc

### Front-end

- [x] `script setup` migration

## Notes

### Doc

- [**Nuxt Typescript**](https://typescript.nuxtjs.org/guide/introduction)
- [**Nuxt Composition API**](https://composition-api.nuxtjs.org/getting-started/introduction)
- [**Windi CSS**](https://windicss.org/guide/)
- [**Nuxt 2**](https://nuxtjs.org/docs/get-started/installation/)
- [**Nuxt 3**](https://v3.nuxtjs.org/docs/usage/data-fetching)
- [**Vue 3 Composition API**](https://v3.vuejs.org/guide/composition-api-introduction.html)
- [**Vue 3 Lifecycle Hooks**](https://v3.vuejs.org/guide/composition-api-lifecycle-hooks.html)
- [**lindsay-wardell blog**](https://www.thisdot.co/author/lindsay-wardell)
- [**Vue 3 script setup props with typescript**](https://blog.ninja-squad.com/2021/09/30/script-setup-syntax-in-vue-3/)
- [**LogRocket**](https://blog.logrocket.com/how-to-set-up-and-code-nuxt-js-apps-fully-in-typescript/)
- [**Bridge**](https://v3.nuxtjs.org/getting-started/bridge/)
- [**@nuxtjs/composition-api discussions**](https://github.com/nuxt-community/composition-api/discussions/585)
- [**Modules status**](https://isnuxt3ready.owln.ai/)
- [**Typography**](https://tailwindcss.com/docs/typography-plugin)
  - [**@apply with dark mode and deep**](https://github.com/tailwindlabs/tailwindcss/issues/6388)
  - [**Add utilities @apply**](https://github.com/tailwindlabs/tailwindcss/issues/5989)
  - [**Plugins**](https://tailwindcss.com/docs/plugins)
  - [**Theme reference**](https://tailwindcss.com/docs/theme#configuration-reference)

### Links

- Webreader: <https://stackoverflow.com/questions/49330858/display-dynamic-html-content-like-an-epub-ebook-without-converting-html-to-epub>
- Cookie consent
  - <https://www.webrankinfo.com/dossiers/droit-internet/consentement-cookies>
  - <https://www.cnil.fr/fr/cookies-et-traceurs-que-dit-la-loi>
  - <https://www.freeprivacypolicy.com/free-cookies-policy-generator/>
  - <https://www.osano.com/cookieconsent/download/>
  - <https://brainsum.github.io/cookieconsent/>
  - <https://github.com/osano/cookieconsent>
  - <https://tarteaucitron.io/en/>
  - <https://debbie.codes/blog/nuxt-cookie-consent/>
  - <https://github.com/EvodiaAut/vue-cookieconsent-component>
  - <https://gitlab.com/broj42/nuxt-cookie-control>
- Nuxt 3
  - <https://medium.com/@AzilenTech/using-script-setup-for-vue-3-ec4b6173b7f4>
  - <https://github.com/nuxt/nuxt.js/issues/5330>: Vue 3 script setup review
  - <https://github.com/nuxt/nuxt.js/issues/7884>
  - <https://robsontenorio.github.io/vue-api-query/configuration>
  - <https://vueschool.io/articles/vuejs-tutorials/nuxt-composition-api/>
  - <https://blog.ninja-squad.com/2021/09/30/script-setup-syntax-in-vue-3>
  - <https://v3.vuejs.org/api/sfc-script-setup.html#defineexpose>
  - <https://al-un.github.io/learn-nuxt-ts>
  - <https://blog.logrocket.com/how-to-set-up-and-code-nuxt-js-apps-fully-in-typescript>
- Vue 3
  - <https://www.thisdot.co/blog/vue-3-2-using-composition-api-with-script-setup>
  - <https://learnvue.co/2020/12/how-to-use-lifecycle-hooks-in-vue3/>
  - <https://v3.vuejs.org/guide/composition-api-introduction.html>
  - <https://v3.vuejs.org/api/composition-api.html>
  - <https://v3.vuejs.org/guide/migration/v-model.html#overview>
  - <https://www.mokkapps.de/blog/why-i-love-vue-3-s-composition-api/>
  - <https://v3.vuejs.org/api/sfc-script-setup.html#using-custom-directives>
  - <https://www.netlify.com/blog/2021/01/29/deep-dive-into-the-vue-composition-apis-watch-method/>
  - <https://vuejsdevelopers.com/2020/03/31/vue-js-form-composition-api/>
  - <https://codecourse.com/watch/build-a-blog-with-laravel-and-vue?part=introduction-and-demo-laravel-vue-blog>
- TipTap
  - <https://tiptap.dev/installation/vue3>
  - <https://bestofvue.com/repo/heyscrumpy-tiptap-vuejs-rich-text-editing>
- JS & Vue
  - <https://www.freecodecamp.org/news/learn-modern-javascript/>
  - <https://stackoverflow.com/questions/63986278/vue-3-v-deep-usage-as-a-combinator-has-been-deprecated-use-v-deepinner-se>

### Code

`axios`

```js
const { data, status } = this.$axios.$get('/api/books').then((e) => e).catch((e) => e)
console.log(data)
console.log(status)
```

`jsonld`

```vue
<script lang="ts">
  import { Component, Vue } from 'vue-property-decorator'
  import { Jsonld } from 'nuxt-jsonld';

  @Jsonld
  @Component
  export default class Sample extends Vue {
    jsonld() {
      return {
        '@context': 'https://schema.org',
        '@type': 'Product'
        name: 'product name',
      };
    }
  };
</script>
```

Tailwind CSS & `v-deep` from [**CSS with scoped and deep**](https://stackoverflow.com/questions/48032006/how-do-i-use-deep-or-or-v-deep-in-vue-js)

```vue
<style lang="css" scoped>
.footer-bottom::v-deep {
  @apply text-gray-500;
  & a {
    @apply hover:text-gray-900 transition-colors duration-100;
  }
}
.dark {
  & .footer-bottom::v-deep {
    @apply text-gray-400;
    & a {
      @apply hover:text-gray-100;
    }
  }
}
</style>
```
