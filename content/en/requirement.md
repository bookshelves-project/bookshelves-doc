---
title: 'Requirements'
description: 'How to setup your environement to work on Bookshelves'
position: 3
category: 'Getting started'
---

You can work on Bookshelves from a fork or offer merge request on [**project repositories**](https://gitlab.com/bookshelves-project).

## Languages

- [**PHP**](https://www.php.net/): `v8.1` for back
- [**Node.js**](https://nodejs.org/en/): `v16.14` for front and back
- [**MySQL**](https://www.mysql.com/): `v8.*` for back

### Extensions

- `php8.1-xml`, `php8.1-gd`
- <badge>optional</badge> `jpegoptim`, `optipng`, `pngquant`, `gifsicle`, `webp`, `svgo`
- <badge>optional</badge> To extract cover from PDF files : `ImageMagick`, PECL `imagick`
- <badge>optional</badge> To parse RAR files : `rar`, PECL `rar`

Check [**Requirements installation**](/requirement-installation) for more details.

## Package managers

- [**pnpm**](https://pnpm.io/): `v6.*` for front and back
- [**Composer**](https://getcomposer.org/): `v2.*` for back

## Frameworks

- [**Nuxt.js**](https://nuxtjs.org/) `v3.*` for front
- [**Laravel**](https://laravel.com/) `v9.*` for back

## Configuration <badge>optional</badge>

- OS: Linux / macOS / Windows
- IDE: [**Visual Studio Code**](https://code.visualstudio.com/)
